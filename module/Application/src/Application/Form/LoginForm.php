<?php
/**
 * Created by PhpStorm.
 * User: ituser
 * Date: 2016-06-24
 * Time: 11:15
 */

namespace Application\Form;


use Zend\Form\Form;

class LoginForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct("Login");

        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'login',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => "Your login",
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Login:'
            )
        ));

        $this->add(array(
            'name' => 'password',
            'type' => 'password',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Your password',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Password:'
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => array(
                'value' => "Login"
            )
        ));
    }

}