<?php
/**
 * Created by PhpStorm.
 * User: ituser
 * Date: 2016-06-16
 * Time: 15:27
 */

namespace Application\Form;

use Zend\Form\Form;

class RegisterForm extends Form{

    public function __construct($name = "formularz", array $options = null)
    {
        parent::__construct($name, $options);

        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'name',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Imie',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Name:'
            )
        ));

        $this->add(array(
            'name' => 'surname',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Nazwisko',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Surname:'
            )
        ));

        $this->add(array(
            'name' => 'email',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'E-mail',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'E-mail:'
            )
        ));

        $this->add(array(
            'type' => 'date',
            'name' => 'birthDate',
            'options' => array(
                'label' => 'BirthDate',
                'format' => 'Y-m-d'
            ),
            'attributes' => array(
                'min' => '1900-01-01',
                'max' => '2020-01-01'
            )
        ));

        $this->add(array(
            'name' => 'login',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Login',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Login:'
            )
        ));

        $this->add(array(
            'name' => 'password',
            'type' => 'password',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Password',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Password:'
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => array(
                'value' => 'Zarejestruj',
                'class' => 'btn-primary'
            )
        ));
    }

}