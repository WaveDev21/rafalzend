<?php
/**
 * Created by PhpStorm.
 * User: ituser
 * Date: 2016-06-23
 * Time: 09:09
 */

namespace Application\Form;


use Zend\InputFilter\InputFilter;

class ContactFilter extends InputFilter
{
    public function __construct()
    {
        $this->add(array(
            'name' => 'imie',
            'required' => true,
            'filters' => array(array(
                'name' => 'StripTags',
            )),
            'validators' => array(array(
                'name' => 'StringLength',
                'options' => array(
                    'min' => 3,
                    'max' => 15,
                )
            ))
        ));
        $this->add(array(
            'name' => 'nazwisko',
            'required' => true,
            'filters' => array(array(
                'name' => 'StripTags',
            )),
            'validators' => array(array(
                'name' => 'StringLength',
                'options' => array(
                    'min' => 3,
                    'max' => 25,
                )
            ))
        ));
        $this->add(array(
            'name' => 'email',
            'required' => true,
            'validators' => array(array(
                'name' => 'EmailAddress',
                'options' => array(
                    'domain' => true
                )
            ))
        ));
    }
}