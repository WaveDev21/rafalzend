<?php
/**
 * Created by PhpStorm.
 * User: ituser
 * Date: 2016-06-16
 * Time: 15:27
 */

namespace Application\Form;

use Zend\Form\Form;

class ContactForm extends Form{

    public function __construct($name = "formularz", array $options = null)
    {
        parent::__construct($name, $options);

        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'imie',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Imie',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Imie:'
            )
        ));

        $this->add(array(
            'name' => 'nazwisko',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Nazwisko',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Nazwisko:'
            )
        ));

        $this->add(array(
            'name' => 'email',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'E-mail',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'E-mail:'
            )
        ));

        $this->add(array(
            'name' => 'wiadomisc',
            'type' => 'textarea',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Twoja wiadomość',
                'required' => 'required',
                'rows' => '5'
            ),
            'options' => array(
                'label' => 'Wiadomość:'
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => array(
                'value' => 'Wyślij wiadomość',
                'class' => 'btn-primary'
            )
        ));
    }

}