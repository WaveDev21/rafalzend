<?php
/**
 * Created by PhpStorm.
 * User: ituser
 * Date: 2016-06-24
 * Time: 11:26
 */

namespace Application\Form;


use Zend\InputFilter\InputFilter;

class LoginFilter extends InputFilter
{
    public function __construct()
    {
        $this->add(array(
            'name' => 'login',
            'required' => true,
            'filters' => array(array(
                'name' => 'StripTags'
            )),
            'validators' => array(array(
                'name' => 'StringLength',
                'options' => array(
                    'min' => 3,
                    'max' => 15
                )
            ))
        ));

        $this->add(array(
            'name' => 'password',
            'required' => true,
            'filters' => array(array(
                'name' => 'StripTags'
            )),
            'validators' => array(array(
                'name' => 'StringLength',
                'options' => array(
                    'min' => 8,
                    'max' => 20
                )
            ))
        ));
    }
}