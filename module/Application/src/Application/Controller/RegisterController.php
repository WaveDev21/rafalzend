<?php
/**
 * Created by PhpStorm.
 * User: ituser
 * Date: 2016-06-23
 * Time: 15:00
 */

namespace Application\Controller;



use Zend\Db\Exception\InvalidArgumentException;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Application\Form\RegisterFilter;
use Application\Form\RegisterForm;



class RegisterController extends AbstractActionController
{
    protected $_userTable;

    # SMTP CONF
    protected $_transport;
    protected $_options;

    # MESSAGE
    protected $_bodyMessage;
    protected $_message;

    # MESSAGE HTML & TXT
    protected $_bodyHTML;
    protected $_bodyTXT;

    public function __construct()
    {
        $this->_transport = new Smtp();
        $this->_options = new SmtpOptions(array(
            'name' => 's78.linuxpl.com',
            'host' => 's78.linuxpl.com',
            'port' => '587',
            'connection_class' => 'login',
            'connection_config' => array(
                'username' => 'kurs@wlodkowski.pl',
                'password' => 'qwer1234'
            )
        ));
    }

    public function indexAction()
    {
        $view = new ViewModel(array(
            'form' => new RegisterForm()
        ));
        return $view;
    }

    public function confirmAction()
    {
        return new ViewModel();
    }

    public function processAction()
    {
        $formData = $this->request->getPost();

        $form =  new RegisterForm();
        $inputFilter = new RegisterFilter();

        $form->setInputFilter($inputFilter);
        $form->setData($formData);

        if(!$form->isValid()){
            $viewModel = new ViewModel(array(
                'error' => true,
                'form' => $form
            ));
            $viewModel->setTemplate('application/register/index');
            return $viewModel;
        }
        
        try{
            $this->getUserTable()->registerUser($formData);    
        }catch (\Exception $e){
            die ($e->getMessage());
        }
        

        // Sending Mail
        $this->sendMail($formData);

        return $this->redirect()->toRoute(NULL, array(
            'controller' => 'register',
            'action' => 'confirm'
        ));

    }

    public function sendMail($formData)
    {

        $this->_bodyMessage = new Mime\Message();
        $this->_bodyMessage->setParts(array($this->createMessageTXT($formData), $this->createMessageHTML($formData)));

        $this->_message = new Message();
        $this->_message
            ->addTo('rafal.polaski@gmail.com')
            ->addFrom('kurs@wlodkowski.pl')
            ->setSubject('Wiadomość z strony www, '.date('Y-m-d H:i'))
            ->setEncoding('UTF-8')
            ->setBody($this->_bodyMessage)
            ->getHeaders()->get('content-type')
            ->setType('multipart/alternative');


        $this->_transport->setOptions($this->_options);
        $this->_transport->send($this->_message);

    }

    protected function createMessageHTML($formData)
    {
        $this->_bodyHTML = '<strong>Od: </strong>'.$formData['name'].' '.$formData['surname'].
            '<br><br><strong> E-mail: </strong>'.$formData['email'].
            '<br><strong> Birth Date: </strong>'.$formData['birthDate'].
            '<br><strong> Login: </strong>'.$formData['login'].
            '<br><strong>Wiadomosc: </strong> Gratulacje zostałes zarejestrowany';

        $htmlPART = new Mime\Part($this->_bodyHTML);
        $htmlPART->charset = 'UTF-8';
        $htmlPART->type = Mime\Mime::TYPE_HTML;
        return $htmlPART;
    }

    protected function createMessageTXT($formData)
    {
        $this->_bodyTXT = 'Od:'.$formData['name'].' '.$formData['surname'].
            'E-mail:'.$formData['email'].
            ' Birth Date: '.$formData['birthDate'].
            ' Login: '.$formData['login'].
            'Wiadomosc: Gratulacje zostałes zarejestrowany';

        $txtPART = new Mime\Part($this->_bodyTXT);
        $txtPART->charset = 'UTF-8';
        $txtPART->type = Mime\Mime::TYPE_TEXT;

        return $txtPART;
    }

    public function getUserTable()
    {
        if(!$this->_userTable){
            $this->_userTable = $this->getServiceLocator()->get('Application\Model\UserTable');
        }
        return $this->_userTable;
    }
}