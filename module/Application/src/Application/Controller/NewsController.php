<?php
/**
 * Created by PhpStorm.
 * User: ituser
 * Date: 2016-06-16
 * Time: 12:35
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Loader;

class NewsController extends AbstractActionController
{
    protected $newsTable;

    public function getNewsTable()
    {
        if(!$this->newsTable){
            $this->newsTable = $this->getServiceLocator()->get('Application\Model\NewsTable');
        }
        return $this->newsTable;
    }

    public function indexAction()
    {
        $lang = $this->getServiceLocator()->get('translator')->getLocale();

        $news = $this->getNewsTable()->getNews($lang);

        return new ViewModel(array(
            'news' => $news
        ));
    }
    
    public function moreAction()
    {
        $id = $this->params()->fromRoute('id');
        $lang = $this->getServiceLocator()->get('translator')->getLocale();

        return new ViewModel(array(
            'topic' => $this->getNewsTable()->getTopic($id, $lang)
        ));
    }
}