<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Zend\View\View;

class IndexController extends AbstractActionController
{
    protected $pageTable;

    public function indexAction()
    {
        //$page_lang = $this->getServiceLocator()->get('translator')->getLocale();
        //$this->getPageTable()->getPage('1', 'pl_PL');

        //$authService = new AuthenticationService();
        //debug($authService->getStorage()->read());
        
        return new ViewModel(array(
            'pageIndexContent' => $this->getPageTable()->getPage('1', 'pl_PL')
        ));
    }

    protected function getPageTable()
    {
        if(!$this->pageTable){
            $sm = $this->getServiceLocator();
            $this->pageTable = $sm->get('Application\Model\PageTable');
        }

        return $this->pageTable;
    }
    
    public function pageAction()
    {
        $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
        $sql = 'Select * FROM t_pages WHERE page_id = 1 and page_lang = \'pl_PL\'';
        $statement = $dbAdapter->query($sql);
        $result = $statement->execute();

        foreach ($result as $r){
            $row = $r;
        }

        return new ViewModel(array(
            'pageIndexContent' => $row
        ));
    }
    public function insertAction()
    {
        $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');

        $sql = "INSERT INTO t_pages VALUES('','SHOW','pl_PL',NOW(),NOW(),'Tytulik','Headerek', 'Jakiś tam contencik')";
        $statement = $dbAdapter->query($sql);
        $result = $statement->execute();


    }
}
