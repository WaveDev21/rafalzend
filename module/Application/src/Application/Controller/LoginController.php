<?php
/**
 * Created by PhpStorm.
 * User: ituser
 * Date: 2016-06-24
 * Time: 11:12
 */

namespace Application\Controller;


use Application\Form\LoginFilter;
use Application\Form\LoginForm;
use Zend\Authentication\AuthenticationService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\Adapter\DbTable\CredentialTreatmentAdapter as DbTableAdapter;
use Zend\View\View;

class LoginController extends AbstractActionController
{
    public $authService;

    public function indexAction()
    {
        $form = new LoginForm();

        return new ViewModel(array(
            'form' => $form
        ));
    }

    public function logoutAction()
    {
        $this->getAuthService()->getStorage()->clear();

        return $this->redirect()->toRoute(NULL, array(
            'controller' => 'index',
            'action' => 'index'
        ));
    }

    public function confirmAction()
    {
        $userLogin = $this->getAuthService()->getStorage()->read();
        
        if(!isset($userLogin)){
            return $this->redirect()->toRoute(NULL, array(
                'controller' => 'login',
                'action' => 'index'
            ));
        }
        return new ViewModel(array(
            'user_login' => $userLogin
        ));
    }

    public function processAction()
    {
        $formData = $this->request->getPost();

        $form = new LoginForm();
        $formFilter = new LoginFilter();

        $form->setInputFilter($formFilter);
        $form->setData($formData);

        if(!$form->isValid()){
            $viewModel = new ViewModel(array(
                'form' => $form,
                'error' => true
            ));
            $viewModel->setTemplate('application/login/index');
            return $viewModel;
        }

        $this->getAuthService()->getAdapter()
            ->setIdentity($formData['login'])
            ->setCredential(md5($formData['password']));

        $result = $this->getAuthService()->authenticate();

        if($result->isValid()){
            $this->getAuthService()->getStorage()->write($formData['login']);
            return $this->redirect()->toRoute(NULL, array('controller' => 'login', 'action' => 'confirm'));
        }

        $viewModel = new ViewModel(array(
            'error' => true,
            'form' => $form
        ));
        $viewModel->setTemplate('application/login/index');
        return $viewModel;
    }

    public function getAuthService()
    {
        if(!$this->authService){
            $dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
            $dbTableAutAdapter = new DbTableAdapter($dbAdapter);
            $dbTableAutAdapter->setTableName('t_uzytkownicy');
            $dbTableAutAdapter->setIdentityColumn('login');
            $dbTableAutAdapter->setCredentialColumn('password');

            $authService = new AuthenticationService();
            $authService->setAdapter($dbTableAutAdapter);
            $this->authService = $authService;
        }
        return $this->authService;
    }

}