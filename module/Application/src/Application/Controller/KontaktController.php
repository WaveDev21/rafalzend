<?php
/**
 * Created by PhpStorm.
 * User: ituser
 * Date: 2016-06-16
 * Time: 11:47
 */

namespace Application\Controller;

use Application\Form\ContactFilter;
use Zend\EventManager\EventManagerInterface;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Form\ContactForm;


class KontaktController extends AbstractActionController
{

    # SMTP CONF
    protected $_transport;
    protected $_options;

    # MESSAGE
    protected $_bodyMessage;
    protected $_message;

    # MESSAGE HTML & TXT
    protected $_bodyHTML;
    protected $_bodyTXT;

    public function __construct()
    {
        $this->_transport = new Smtp();
        $this->_options = new SmtpOptions(array(
            'name' => 's78.linuxpl.com',
            'host' => 's78.linuxpl.com',
            'port' => '587',
            'connection_class' => 'login',
            'connection_config' => array(
                'username' => 'kurs@wlodkowski.pl',
                'password' => 'qwer1234'
            )
        ));
    }

    protected function createMessageHTML($formData)
    {
        $this->_bodyHTML = '<strong>Od: </strong>'.$formData['imie'].' '.$formData['nazwisko'].
            '<br><br><strong> E-mail: </strong>'.$formData['email'].
            '<br><strong>Wiadomosc: </strong>'.$formData['wiadomisc'];

        $htmlPART = new Mime\Part($this->_bodyHTML);
        $htmlPART->charset = 'UTF-8';
        $htmlPART->type = Mime\Mime::TYPE_HTML;
        return $htmlPART;
    }

    protected function createMessageTXT($formData)
    {
        $this->_bodyTXT = 'Od:'.$formData['imie'].' '.$formData['nazwisko'].
            'E-mail:'.$formData['email'].
            'Wiadomosc: '.$formData['wiadomisc'];

        $txtPART = new Mime\Part($this->_bodyTXT);
        $txtPART->charset = 'UTF-8';
        $txtPART->type = Mime\Mime::TYPE_TEXT;

        return $txtPART;
    }

    public function indexAction()
    {

        $formularz = new ContactForm();
        return new ViewModel(array(
            'formularz' => $formularz,
        ));
    }

    public function sendAction()
    {
        $formData = $this->request->getPost();

        $form =  new ContactForm();
        $inputFilter = new ContactFilter();

        $form->setInputFilter($inputFilter);
        $form->setData($formData);

        if(!$form->isValid()){
            $viewModel = new ViewModel(array(
                'error' => true,
                'formularz' => $form
            ));
            $viewModel->setTemplate('application/kontakt/index');
            return $viewModel;
        }

        $this->_bodyMessage = new Mime\Message();
        $this->_bodyMessage->setParts(array($this->createMessageTXT($formData), $this->createMessageHTML($formData)));
        
        $this->_message = new Message();
        $this->_message
            ->addTo('rafal.polaski@gmail.com')
            ->addFrom('kurs@wlodkowski.pl')
            ->setSubject('Wiadomość z strony www, '.date('Y-m-d H:i'))
            ->setEncoding('UTF-8')
            ->setBody($this->_bodyMessage)
                ->getHeaders()->get('content-type')
                ->setType('multipart/alternative');


        $this->_transport->setOptions($this->_options);
        $this->_transport->send($this->_message);

        return new ViewModel();
    }

    public function setEventManager(EventManagerInterface $events)
    {
        parent::setEventManager($events);
        $controller = $this;
        $events->attach('dispatch', function ($e) use ($controller){
            $controller->layout('layout/layout-custom');
        }, 100);
    }

}