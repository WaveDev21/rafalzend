<?php
/**
 * Created by PhpStorm.
 * User: ituser
 * Date: 2016-06-24
 * Time: 14:20
 */

namespace Application\Model;


class NewsModel
{
    public $id;
    public $lang;
    public $title;
    public $header;
    public $content;
    public $autor;
    public $create_date;
    public $update_date;

    public function exchangeArray($data)
    {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->lang = (!empty($data['lang'])) ? $data['lang'] : null;
        $this->title = (!empty($data['title'])) ? $data['title'] : null;
        $this->header = (!empty($data['header'])) ? $data['header'] : null;
        $this->content = (!empty($data['content'])) ? $data['content'] : null;
        $this->autor = (!empty($data['autor'])) ? $data['autor'] : null;
        $this->create_date = (!empty($data['create_date'])) ? $data['create_date'] : null;
        $this->update_date = (!empty($data['update_date'])) ? $data['update_date'] : null;
    }
}