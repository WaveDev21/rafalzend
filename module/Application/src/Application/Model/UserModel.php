<?php
/**
 * Created by PhpStorm.
 * User: ituser
 * Date: 2016-06-24
 * Time: 08:19
 */

namespace Application\Model;


class UserModel
{
    public $id;
    public $login;
    public $email;
    public $name;
    public $surname;
    public $create_date;
    public $status;
    public $password;
    public $birthDate;

    public function exchangeArray($data)
    {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->login = (!empty($data['login'])) ? $data['login'] : null;
        $this->password = (!empty($data['password'])) ? md5($data['password']) : null;
        $this->email = (!empty($data['email'])) ? $data['email'] : null;
        $this->name = (!empty($data['name'])) ? $data['name'] : null;
        $this->surname = (!empty($data['surname'])) ? $data['surname'] : null;
        $this->birthDate = (!empty($data['birth_date'])) ? $data['birth_date'] : null;
        $this->create_date = (!empty($data['create_date'])) ? $data['create_date'] : null;
        $this->status = (!empty($data['status'])) ? $data['status'] : null;
    }
}