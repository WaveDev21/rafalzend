<?php
/**
 * Created by PhpStorm.
 * User: ituser
 * Date: 2016-06-24
 * Time: 14:25
 */

namespace Application\Model;


use Zend\Db\TableGateway\TableGateway;

class NewsTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getNews($news_lang)
    {
        $news_lang = (string) $news_lang;

        $rows = $this->tableGateway->select(array(
            'lang' => $news_lang
        ));


        if(!$rows){
            throw new \Exception('Brak aktualnosci');
        }

        return $rows;
    }

    public function getTopic($id, $lang){
        $id = (int) $id;
        $lang = (string) $lang;

        $rows = $this->tableGateway->select(array(
            'id' => $id,
            'lang' => $lang
        ));

        if($rows->count() == 0){
            throw new \Exception('Brak aktualnosci');
        }

        return $rows->current();
    }
}