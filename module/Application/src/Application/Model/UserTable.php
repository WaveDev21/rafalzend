<?php
/**
 * Created by PhpStorm.
 * User: ituser
 * Date: 2016-06-24
 * Time: 08:19
 */

namespace Application\Model;


use Zend\Db\TableGateway\TableGateway;

class UserTable
{
    protected $_tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->_tableGateway = $tableGateway;
    }

    public function registerUser($userData)
    {

        
        $data = array(
            'login' => $userData->login,
            'password' => md5($userData->password),
            'email' => $userData->email,
            'name' => $userData->name,
            'surname' => $userData->surname,
            'birth_date' => $userData->birthDate
        );

        $this->_tableGateway->insert($data);
    }
}