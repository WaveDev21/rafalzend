<?php
/**
 * Created by PhpStorm.
 * User: ituser
 * Date: 2016-06-23
 * Time: 13:18
 */

namespace Application\Model;


use Zend\Db\TableGateway\TableGateway;

class PageTable
{
    protected $_tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->_tableGateway = $tableGateway;
    }

    public function getPage($page_id, $page_lang)
    {   
        $page_id = (int) $page_id;
        $page_lang = (string) $page_lang;

        $result = $this->_tableGateway->select(array(
            'page_id' => $page_id,
            'page_lang' => $page_lang
        ));
        $row = $result->current();

        if(!$row)
        {
            throw new \Exception('Brak takiej strony');
        }

        return $row;

    }

}