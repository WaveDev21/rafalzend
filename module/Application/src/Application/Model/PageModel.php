<?php
/**
 * Created by PhpStorm.
 * User: ituser
 * Date: 2016-06-23
 * Time: 13:13
 */

namespace Application\Model;


class PageModel
{
    public $page_title;
    public $page_header;
    public $page_content;
    public $page_update_date;

    public function exchangeArray($data)
    {
        $this->page_title = (!empty($data['page_title'])) ? $data['page_title'] : null;
        $this->page_header = (!empty($data['page_header'])) ? $data['page_header'] : null;
        $this->page_content = (!empty($data['page_content'])) ? $data['page_content'] : null;
        $this->page_update_date = (!empty($data['page_update_date'])) ? $data['page_update_date'] : null;
    }
}