<?php
/**
 * Created by PhpStorm.
 * User: ituser
 * Date: 2016-06-09
 * Time: 13:42
 */

function debug($var, $rodzaj = '')
{
    switch ($rodzaj) {
        case 'short':
            echo '<blockquote style="padding: 10px; background-color: darkgray; border-radius: 5px"><pre>';
            print_r ( $var );
            echo '</pre></blockquote>';
            break;
        default:
            echo '<blockquote style="padding: 10px; background-color: darkgray; border-radius: 5px"><pre>';
            var_dump ( $var );
            echo '</pre></blockquote>';
    }
}